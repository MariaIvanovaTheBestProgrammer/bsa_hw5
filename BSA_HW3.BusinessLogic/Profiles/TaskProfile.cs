﻿using AutoMapper;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;

namespace BSA_HW3.BusinessLogic.Profiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.TaskName));
            CreateMap<TaskDTO, Task>()
                .ForMember(dest => dest.Project, opt => opt.Ignore())
                .ForMember(dest => dest.TaskName, opt => opt.MapFrom(src => src.Name));
        }
    }
}
