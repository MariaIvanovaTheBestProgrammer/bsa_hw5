﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface ITaskService
    {
        IEnumerable<TaskDTO> GetTasks();
        TaskDTO GetTaskById(int taskId);
        void CreateTask(TaskDTO taskDTO);
        void DeleteTask(int taskId);
        void UpdateTask(TaskDTO taskDTO);
        IEnumerable<TaskDTO> GetUserTasks(int userId);
        IEnumerable<TaskInfo> GetFinishedTaskIdName(int userId);
    }
}
