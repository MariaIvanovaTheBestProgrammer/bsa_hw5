﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetTeams();
        TeamDTO GetTeamById(int teamId);
        TeamDTO CreateTeam(TeamDTO teamDTO);
        void DeleteTeam(int teamId);
        void UpdateTeam(TeamDTO teamDTO);
        IEnumerable<TeamInfo> GetSortedTeamsInfo();
    }
}
