﻿using BSA_HW3.Common;
using System.Collections.Generic;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetProjects();
        ProjectDTO GetProjectById(int projectId);
        ProjectDTO CreateProject(ProjectDTO projectDTO);
        void DeleteProject(int projectId);
        void UpdateProject(ProjectDTO projectDTO);
        IEnumerable<KeyValuePair<ProjectDTO, int>> GetProjectToTasksCountDictionary(int authorId);
        IEnumerable<ProjectInfo> GetProjectInfo();
    }
}
