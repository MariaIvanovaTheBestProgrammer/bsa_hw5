﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using System.Collections.Generic;

namespace BSA_HW3.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetUsers();
        UserDTO GetUserById(int userId);
        UserDTO CreateUser(UserDTO userDTO);
        void DeleteUser(int userId);
        void UpdateUser(UserDTO userDTO);
        IEnumerable<User> GetUsersSortedByName();
        UserProjectInfo GetUserProjectInfo(int userId);
    }
}
