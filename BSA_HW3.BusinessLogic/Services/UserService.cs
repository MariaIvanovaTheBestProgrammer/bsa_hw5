﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _iMapper;

        public UserService(IUserRepository userRepository,
            ITaskRepository taskRepository,
            IProjectRepository projectRepository,
            IMapper iMapper
            )
        {
            _userRepository = userRepository;
            _projectRepository = projectRepository;
            _taskRepository = taskRepository;
            _iMapper = iMapper;
        }

        public UserDTO CreateUser(UserDTO userDTO)
        {
            var entity = _iMapper.Map<UserDTO, User>(userDTO);
            _userRepository.CreateUser(entity);
            return _iMapper.Map<User, UserDTO>(entity);
        }

        public void DeleteUser(int userId)
        {
            _userRepository.DeleteUser(userId);
        }

        public UserDTO GetUserById(int userId)
        {
            return _iMapper.Map<UserDTO>(_userRepository.GetUserById(userId));
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            return _userRepository.GetUsers().Select(e => _iMapper.Map<UserDTO>(e));
        }

        public void UpdateUser(UserDTO userDTO)
        {
            _userRepository.UpdateUser(_iMapper.Map<UserDTO, User>(userDTO));
        }

        //5 linq
        public IEnumerable<User> GetUsersSortedByName()
        {
            var users = _userRepository.GetUsers();
            var tasks = _taskRepository.GetTasks();
            users = users.GroupJoin(
                                    tasks,
                                    u => u.Id,
                                    t => t.PerformerId,
                                    (u, t) =>
                                    {
                                        u.Tasks = t;
                                        return u;
                                    }
                        );
            return users.OrderBy(u => u.FirstName)
                .Select(u => {
                    u.Tasks = u.Tasks.OrderByDescending(t => t.TaskName.Length);
                    return u;
                });
        }

        //6 linq
        public UserProjectInfo GetUserProjectInfo(int userId)
        {
            var users = _userRepository.GetUsers();
            var tasks = _taskRepository.GetTasks();
            var projects = _projectRepository.GetProjects();
            users = users.GroupJoin(
                                    tasks,
                                    u => u.Id,
                                    t => t.PerformerId,
                                    (u, t) =>
                                    {
                                        u.Tasks = t;
                                        return u;
                                    }
                        )
                        .GroupJoin(
                                    projects,
                                    u => u.Id,
                                    p => p.UserId,
                                    (u, p) =>
                                    {
                                        u.Projects = p.GroupJoin(
                                                                  tasks,
                                                                  p => p.Id,
                                                                  t => t.ProjectId,
                                                                  (p, t) =>
                                                                  {
                                                                      p.Tasks = t;
                                                                      return p;
                                                                  }
                                                                );
                                        return u;
                                    });
            return users.Where(u => u.Id == userId)
                .Select(u => new UserProjectInfo
                {
                    User = u,
                    LastCreatedProject = u.Projects.OrderByDescending(p => p.CreatedAt).FirstOrDefault(),
                    AllTasksCount = u.Projects.OrderByDescending(p => p.CreatedAt)
                                                    .FirstOrDefault()
                                                    ?.Tasks?.Count() ?? 0,
                    CancelledOrInProgressTasksCount = u.Tasks.Where(t => t.TaskState != TaskState.Done || t.TaskState == TaskState.Canceled).Count(),
                    LongestTask = u.Tasks.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault()
                })
                .SingleOrDefault();
        }
    }
}
