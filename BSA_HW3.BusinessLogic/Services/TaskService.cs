﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.BusinessLogic.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _iMapper;

        public TaskService(ITaskRepository taskRepository, IMapper iMapper)
        {
            _taskRepository = taskRepository;
            _iMapper = iMapper;
        }

        public void CreateTask(TaskDTO taskDTO)
        {
            var entity = _iMapper.Map<TaskDTO, Task>(taskDTO);
            _taskRepository.CreateTask(entity);
        }

        public void DeleteTask(int taskId)
        {
            _taskRepository.DeleteTask(taskId);
        }

        public TaskDTO GetTaskById(int taskId)
        {
            return _iMapper.Map<TaskDTO>(_taskRepository.GetTaskById(taskId));
        }

        public IEnumerable<TaskDTO> GetTasks()
        {
            return _taskRepository.GetTasks().Select(e => _iMapper.Map<TaskDTO>(e));
        }

        public void UpdateTask(TaskDTO taskDTO)
        {
            _taskRepository.UpdateTask(_iMapper.Map<TaskDTO, Task>(taskDTO));
        }

        //2 linq
        public IEnumerable<TaskDTO> GetUserTasks(int userId)
        {
            return _taskRepository.GetTasks()
                .Where(t => t.PerformerId == userId && t.TaskName.Length < 45)
                .Select(e => _iMapper.Map<TaskDTO>(e));
        }

        //3 linq
        public IEnumerable<TaskInfo> GetFinishedTaskIdName(int userId)
        {
            return _taskRepository.GetTasks()
                .Where(t => t.PerformerId == userId)
                .Where(t => t.TaskState == TaskState.Done && t.FinishedAt?.Year == 2021)
                .Select(t => new TaskInfo { Id = t.TaskId, Name = t.TaskName });
        }
    }
}
