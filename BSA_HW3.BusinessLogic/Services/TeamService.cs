﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.BusinessLogic.Services
{
    public class TeamService : ITeamService
    {
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _iMapper;

        public TeamService(ITeamRepository teamRepository, IUserRepository userRepository, IMapper iMapper)
        {
            _teamRepository = teamRepository;
            _userRepository = userRepository;
            _iMapper = iMapper;
        }

        public TeamDTO CreateTeam(TeamDTO teamDTO)
        {
            var entity = _iMapper.Map<TeamDTO, Team>(teamDTO);
            _teamRepository.CreateTeam(entity);
            return _iMapper.Map<Team, TeamDTO>(entity);
        }

        public void DeleteTeam(int teamId)
        {
            _teamRepository.DeleteTeam(teamId);
        }

        public TeamDTO GetTeamById(int teamId)
        {
            return _iMapper.Map<TeamDTO>(_teamRepository.GetTeamById(teamId));
        }

        public IEnumerable<TeamDTO> GetTeams()
        {
            return _teamRepository.GetTeams().Select(e => _iMapper.Map<TeamDTO>(e));
        }

        public void UpdateTeam(TeamDTO teamDTO)
        {
            _teamRepository.UpdateTeam(_iMapper.Map<TeamDTO, Team>(teamDTO));
        }

        //4 linq
        public IEnumerable<TeamInfo> GetSortedTeamsInfo()
        {
            var users = _userRepository.GetUsers();
            var teams = _teamRepository.GetTeams();
            teams = teams.GroupJoin(
                                    users,
                                    t => t.Id,
                                    u => u.TeamId,
                                    (t, u) =>
                                    {
                                        t.Users = u;
                                        return t;
                                    }
                                    );
            return teams.Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDay.Year >= 10))
                .Select(t => new TeamInfo
                {
                    Id = t.Id,
                    Name = t.TeamName,
                    Users = t.Users.OrderByDescending(u => u.RegisteredAt)
                });
        }
    }
}
