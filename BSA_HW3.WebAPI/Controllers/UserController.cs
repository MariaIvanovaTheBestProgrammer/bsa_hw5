﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUsers()
        {
            return Ok(_userService.GetUsers());
        }
        [HttpGet("{userId}")]
        public ActionResult<UserDTO> GetUserById(int userId)
        {
            try
            {
                return Ok(_userService.GetUserById(userId));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

        }
        [HttpDelete("{userId}")]
        public IActionResult DeleteUser(int userId)
        {
            _userService.DeleteUser(userId);
            return NoContent();
        }
        [HttpPost]
        public IActionResult CreateUser(UserDTO userDTO)
        {
            return StatusCode(201, _userService.CreateUser(userDTO));
        }
        [HttpPut]
        public IActionResult UpdateUser(UserDTO userDTO)
        {
            _userService.UpdateUser(userDTO);
            return Ok(_userService.GetUserById(userDTO.Id));
        }
        [HttpGet("SortedTasks")]
        public ActionResult<IEnumerable<User>> GetUsersSortedByName()
        {
            return Ok(_userService.GetUsersSortedByName());
        }
        [HttpGet("UserProjectInfo/{userId}")]
        public ActionResult<UserProjectInfo> GetUserProjectInfo(int userId)
        {
            return Ok(_userService.GetUserProjectInfo(userId));
        }
    }
}
