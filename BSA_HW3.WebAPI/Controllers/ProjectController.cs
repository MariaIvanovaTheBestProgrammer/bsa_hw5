﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }
        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjects()
        {
            return Ok(_projectService.GetProjects());
        }
        [HttpGet("{projectId}")]
        public ActionResult<ProjectDTO> GetProjectById(int projectId)
        {
            try
            {
                return Ok(_projectService.GetProjectById(projectId));
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpDelete("{projectId}")]
        public IActionResult DeleteProject(int projectId)
        {
            try
            {
                _projectService.DeleteProject(projectId);
                return NoContent();
            }catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
        }
        [HttpPost]
        public IActionResult CreateProject(ProjectDTO projectDTO)
        {
            return StatusCode(201, _projectService.CreateProject(projectDTO));
        }
        [HttpPut]
        public IActionResult UpdateProject(ProjectDTO projectDTO)
        {
            _projectService.UpdateProject(projectDTO);
            return Ok(_projectService.GetProjectById(projectDTO.Id));
        }
        [HttpGet("ProjectToTasksCount/{authorId}")]
        public ActionResult<IEnumerable<KeyValuePair<Project, int>>> GetProjectToTasksCountDictionary(int authorId)
        {
            return Ok(_projectService.GetProjectToTasksCountDictionary(authorId));
        }
        [HttpGet("Info")]
        public ActionResult<IEnumerable<ProjectInfo>> GetProjectInfo()
        {
            return Ok(_projectService.GetProjectInfo());
        }

    }
}
