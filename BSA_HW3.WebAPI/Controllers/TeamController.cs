﻿using BSA_HW3.BusinessLogic.Interfaces;
using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BSA_HW3.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        [HttpGet("teams")]
        public ActionResult<IEnumerable<TeamDTO>> GetTeams()
        {
            return Ok(_teamService.GetTeams());
        }
        [HttpGet("{teamId}")]
        public ActionResult<TeamDTO> GetTeamById(int teamId)
        {
            return Ok(_teamService.GetTeamById(teamId));
        }
        [HttpDelete("{teamId}")]
        public IActionResult DeleteTeam(int teamId)
        {
            _teamService.DeleteTeam(teamId);
            return NoContent();
        }
        [HttpPost]
        public IActionResult CreateTeam(TeamDTO teamDTO)
        {
            return StatusCode(201, _teamService.CreateTeam(teamDTO));
        }
        [HttpPut]
        public IActionResult UpdateTeam(TeamDTO teamDTO)
        {
            _teamService.UpdateTeam(teamDTO);
            return Ok(_teamService.GetTeamById(teamDTO.Id));
        }
        [HttpGet("SortedTeams")]
        public ActionResult<IEnumerable<TeamInfo>> GetSortedTeamsInfo()
        {
            return Ok(_teamService.GetSortedTeamsInfo());
        }
    }
}
