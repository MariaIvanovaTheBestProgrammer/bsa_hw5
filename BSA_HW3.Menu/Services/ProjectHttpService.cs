﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Menu.Services
{
    public class ProjectHttpService
    {
        static readonly HttpClient client = new HttpClient();

        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "project");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<ProjectDTO> GetProjectById(int projectId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"project/{projectId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<ProjectDTO>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<Dictionary<ProjectDTO, int>> GetProjectToTasksCountDictionary(int authorId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"project/ProjectToTasksCount/{authorId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<IEnumerable<ProjectInfo>> GetProjectInfo()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "project/Info");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<ProjectInfo>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task CreateProject (ProjectDTO projectDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(projectDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Settings.basePath + "project", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task DeleteProject(int projectId)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(Settings.basePath + $"project/{projectId}");
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task UpdateProject(ProjectDTO projectDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(projectDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(Settings.basePath + "project", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}
