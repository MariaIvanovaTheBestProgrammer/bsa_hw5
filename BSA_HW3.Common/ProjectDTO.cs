﻿using System;

namespace BSA_HW3.Common
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Descriprion { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

    }
}
