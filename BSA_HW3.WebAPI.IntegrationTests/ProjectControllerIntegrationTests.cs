using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.WebAPI.IntegrationTests
{
    public class ProjectControllerIntegrationTests : 
        IClassFixture<CustomWebApplicationFactory<Startup>>,
        IDisposable
    {
        private readonly HttpClient _client;

        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task AddProject_ThanResponseWithCode201AndCorrespondedBody()
        {
            var projectDTO = new ProjectDTO {
                Id = 0, 
                Name = "testproject",
                Descriprion = "test",
                TeamId = 1,
                AuthorId = 1,
                CreatedAt = DateTime.Now
            };
            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            var httpResponse = await _client.PostAsync("api/project",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(projectDTO.Name, createdProject.Name);
            Assert.Equal(projectDTO.Descriprion, createdProject.Descriprion);
        }

        private async Task<int> AddProject(int id)
        {
            var projectDTO = new ProjectDTO {
                Id = id,
                Name = "testproject",
                AuthorId = 0,
                TeamId = 0,
                CreatedAt = DateTime.Now,
                Descriprion = "test"
            };
            string jsonInString = JsonConvert.SerializeObject(projectDTO);
            var httpResponse = await _client.PostAsync("api/project",
                new StringContent(jsonInString, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            return createdProject.Id;
            
        }

        [Fact]
        public async Task DeleteProject_ThanResponseCode204AndGetByIdResponseCode404()
        {
            var id = await AddProject(0);
            var httpResponse = await _client.DeleteAsync($"api/project/{id}");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/project/{id}");
            Assert.Equal(HttpStatusCode.NoContent, httpResponseGetById.StatusCode);
        }        
    }
}
