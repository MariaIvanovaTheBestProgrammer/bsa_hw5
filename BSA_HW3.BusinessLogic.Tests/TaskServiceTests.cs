﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Services;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSA_HW3.BusinessLogic.Tests
{
    public class TaskServiceTests
    {
        readonly TaskService _taskService;
        private readonly ITaskRepository taskRepository;
        private readonly IMapper mapper;
        public TaskServiceTests()
        {
            taskRepository = A.Fake<ITaskRepository>();
            mapper = A.Fake<IMapper>();

            _taskService = new TaskService(
                taskRepository,
                mapper
                );
        }
        //linq2
        [Test]
        public void GetUsersTsk_GetTasksByUserId()
        {
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                    PerformerId = 0,
                    TaskName = "task1"
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                    TaskName = "task2"
                }
            };
            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);
            A.CallTo(() => mapper.Map<TaskDTO>(A<Task>.Ignored)).ReturnsLazily((object o) =>
            {
                if (o is Task t)
                    return new TaskDTO { 
                        TaskId = t.TaskId,
                        ProjectId = t.ProjectId,
                        PerformerId  = t.PerformerId,};
                return null;
            });

            var result = _taskService.GetUserTasks(1);
            Assert.That(result.Select(item => item.PerformerId), Is.All.EqualTo(1));
        }

        [Test]
        public void GetUsersTsk_GetTasksByUserNameLength()
        {
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                    PerformerId = 0,
                    TaskName = "task1"
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                    TaskName = "task2"
                }
            };
            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);
            A.CallTo(() => mapper.Map<TaskDTO>(A<Task>.Ignored)).ReturnsLazily((object o) =>
            {
                if (o is Task t)
                    return new TaskDTO
                    {
                        TaskId = t.TaskId,
                        ProjectId = t.ProjectId,
                        PerformerId = t.PerformerId,
                        Name = t.TaskName,
                    };
                return null;
            });

            var result = _taskService.GetUserTasks(1);
            Assert.That(result.Select(item => item.Name.Length), Is.All.EqualTo(5));
        }

        //linq3
        [Test]
        public void GetFinishedTaskIdName_GetTasksByUserId()
        {
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                    PerformerId = 0,
                    TaskName = "task1",
                    TaskState = TaskState.ToDo,
                    FinishedAt = DateTime.Now
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                    TaskName = "task2",
                    TaskState = TaskState.Done,
                    FinishedAt = DateTime.Now
                }
            };

            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);
            A.CallTo(() => mapper.Map<TaskDTO>(A<Task>.Ignored)).ReturnsLazily((object o) =>
            {
                if (o is Task t)
                    return new TaskDTO
                    {
                        TaskId = t.TaskId,
                        ProjectId = t.ProjectId,
                        PerformerId = t.PerformerId,
                        Name = t.TaskName,
                        TaskState = t.TaskState,
                        FinishedAt = t.FinishedAt
                    };
                return null;
            });

            var result = _taskService.GetFinishedTaskIdName(1);
            Assert.That(result.Select(item => item.Id), Is.All.EqualTo(1));
        }

        [Test]
        public void GetFinishedTaskIdName_GetTasksByFinishedYear()
        {
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                    PerformerId = 0,
                    TaskName = "task1",
                    TaskState = TaskState.ToDo,
                    FinishedAt = DateTime.Now
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 1,
                    PerformerId = 1,
                    TaskName = "task2",
                    TaskState = TaskState.Done,
                    FinishedAt = DateTime.Now
                }
            };

            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);
            A.CallTo(() => mapper.Map<TaskDTO>(A<Task>.Ignored)).ReturnsLazily((object o) =>
            {
                if (o is Task t)
                    return new TaskDTO
                    {
                        TaskId = t.TaskId,
                        ProjectId = t.ProjectId,
                        PerformerId = t.PerformerId,
                        Name = t.TaskName,
                        TaskState = t.TaskState,
                        FinishedAt = t.FinishedAt
                    };
                return null;
            });

            var result = _taskService.GetFinishedTaskIdName(1);
            Assert.That(result.Select(item => item.Name), Is.All.EqualTo("task2"));
        }
    }
    
}
