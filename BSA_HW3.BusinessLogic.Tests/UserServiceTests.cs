﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Services;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using FakeItEasy;
using NUnit.Framework;
using System.Collections.Generic;
namespace BSA_HW3.BusinessLogic.Tests
{
    [TestFixture]
    public class UserServiceTests
    {
        readonly UserService _userService;
        private readonly IProjectRepository projectRepository;
        private readonly ITaskRepository taskRepository;
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;

        public UserServiceTests()
        {
            projectRepository = A.Fake<IProjectRepository>();
            taskRepository = A.Fake<ITaskRepository>();
            userRepository = A.Fake<IUserRepository>();
            mapper = A.Fake<IMapper>();

            _userService = new UserService(
                userRepository,
                taskRepository,
                projectRepository,
                mapper
                );
        }
        [Test]
        public void CreateUser_CallsMapperAndRepository()
        {
            var userDTO = A.Fake<UserDTO>();
            var user = A.Fake<User>();
            A.CallTo(() => mapper.Map<UserDTO, User>(userDTO)).Returns(user);
            _userService.CreateUser(userDTO);
            A.CallTo(() => mapper.Map<UserDTO, User>(userDTO)).MustHaveHappened();
            A.CallTo(() => userRepository.CreateUser(user)).MustHaveHappened();
        }

        //linq6
        [Test]
        public void GetUserProjectInfo_GetById()
        {
            var mockProjects = new List<Project>()
            {
                new Project
                {
                    Id = 0,
                    UserId = 1,

                },
                new Project
                {
                    Id = 1,
                    UserId = 2,
                }
            };
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                    TaskName = "test"
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 0,
                    TaskName = "testtest"
                }
            };
            var mockUsers = new List<User>()
            {
                new User
                {
                   Id = 0,
                   TeamId = 0
                },
                new User
                {
                   Id = 1,
                   TeamId = 0
                }
            };
            A.CallTo(() => projectRepository.GetProjects()).Returns(mockProjects);
            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);
            A.CallTo(() => userRepository.GetUsers()).Returns(mockUsers);

            var result = _userService.GetUserProjectInfo(1);
            Assert.That(result.User.Id, Is.EqualTo(1));
        }
    }
}
