﻿using AutoMapper;
using BSA_HW3.BusinessLogic.Services;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using FakeItEasy;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA_HW3.BusinessLogic.Tests
{
    public class TeamServiceTests
    {
        readonly TeamService _teamService;
        private readonly ITeamRepository teamRepository;
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;

        public TeamServiceTests()
        {
            teamRepository = A.Fake<ITeamRepository>();
            userRepository = A.Fake<IUserRepository>();
            mapper = A.Fake<IMapper>();

            _teamService = new TeamService(
                teamRepository,
                userRepository,
                mapper
                );
        }

        //linq4
        [Test]
        public void GetSortedTeamsInfo()
        {
            var mockUsers = new List<User>()
            {
                new User
                {
                   Id = 0,
                   TeamId = 0,
                   BirthDay = new DateTime(2001, 05, 03),
                },
                new User
                {
                   Id = 1,
                   TeamId = 0,
                   BirthDay = new DateTime(2020, 11, 11)
                }
            };
            var mockTeams = new List<Team>()
            {
                new Team
                {
                    Id = 0,
                    TeamName = "testteam1"
                }
            };

            A.CallTo(() => teamRepository.GetTeams()).Returns(mockTeams);
            A.CallTo(() => userRepository.GetUsers()).Returns(mockUsers);

            var result = _teamService.GetSortedTeamsInfo();
            Assert.That(result.Select(item => item.Users.First().BirthDay.Year - DateTime.Now.Year), Is.All.GreaterThan(10));
        }
    }
    
}
