using AutoMapper;
using BSA_HW3.BusinessLogic.Services;
using BSA_HW3.Common;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using FakeItEasy;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.BusinessLogic.Tests
{
    [TestFixture]
    public class ProjectServiceTests
    {
        readonly ProjectService _projectService;
        private readonly IProjectRepository projectRepository;
        private readonly ITaskRepository taskRepository;
        private readonly ITeamRepository teamRepository;
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        public ProjectServiceTests()
        {
            projectRepository = A.Fake<IProjectRepository>();
            taskRepository = A.Fake<ITaskRepository>();
            teamRepository = A.Fake<ITeamRepository>();
            userRepository = A.Fake<IUserRepository>();
            mapper = A.Fake<IMapper>();

            _projectService = new ProjectService(
                projectRepository,
                taskRepository,
                teamRepository,
                userRepository,
                mapper
                );
        }

        [Test]
        public void CreateProject_CallsMapperAndRepository()
        {
            var projectDTO = A.Fake<ProjectDTO>();
            var project = A.Fake<Project>();
            A.CallTo(() => mapper.Map<ProjectDTO, Project>(projectDTO)).Returns(project);
            A.CallTo(() => mapper.Map<Project, ProjectDTO>(project)).Returns(projectDTO);
            _projectService.CreateProject(projectDTO);
            A.CallTo(() => mapper.Map<ProjectDTO, Project>(projectDTO)).MustHaveHappened();
            A.CallTo(() => projectRepository.CreateProject(project)).MustHaveHappened();
        }
        //linq1
        [Test]
        public void GetProjectsToTasksCont_GetProjectsByUserId()
        {
            var mockProjects = new List<Project>()
            {
                new Project
                {
                    Id = 0,
                    UserId = 1,

                },
                new Project
                {
                    Id = 1,
                    UserId = 2,
                }
            };
            var mockTasks = new List<Task>()
            {
            };
            A.CallTo(() => mapper.Map<ProjectDTO>(A<Project>.Ignored)).ReturnsLazily((object o) =>
            {
                if (o is Project p)
                    return new ProjectDTO { Id = p.Id, AuthorId = p.UserId };
                return null;
            });
            A.CallTo(() => projectRepository.GetProjects()).Returns(mockProjects);
            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);

            var result = _projectService.GetProjectToTasksCountDictionary(1);
            Assert.That(result.Select(item => item.Key.AuthorId), Is.All.EqualTo(1));
        }

        [Test]
        public void GetProjectsToTasksCont_GetTasksCount()
        {
            var mockProjects = new List<Project>()
            {
                new Project
                {
                    Id = 0,
                    UserId = 1,

                },
                new Project
                {
                    Id = 1,
                    UserId = 2,
                }
            };
            var mockTasks = new List<Task>()
            {
                new Task
                {
                    TaskId = 0,
                    ProjectId = 0,
                },
                new Task
                {
                    TaskId = 1,
                    ProjectId = 1,
                }
            };

            A.CallTo(() => projectRepository.GetProjects()).Returns(mockProjects);
            A.CallTo(() => taskRepository.GetTasks()).Returns(mockTasks);

            var result = _projectService.GetProjectToTasksCountDictionary(1);
            Assert.That(result.Select(item => item.Value), Is.All.EqualTo(1));
        }

    }
}
