﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BSA_HW3.Data.Migrations
{
    public partial class ColumnNameChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Team",
                newName: "TeamName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Task",
                newName: "TaskName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Project",
                newName: "ProjectName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TeamName",
                table: "Team",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "TaskName",
                table: "Task",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ProjectName",
                table: "Project",
                newName: "Name");
        }
    }
}
