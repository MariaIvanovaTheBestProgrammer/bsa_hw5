﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;

namespace BSA_HW3.Data.Interfaces
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int userId);
        void CreateUser(User user);
        void DeleteUser(int userId);
        void UpdateUser(User user);
    }
}
