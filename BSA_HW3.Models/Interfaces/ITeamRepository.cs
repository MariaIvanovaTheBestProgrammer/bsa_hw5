﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;


namespace BSA_HW3.Data.Interfaces
{
    public interface ITeamRepository
    {
        IEnumerable<Team> GetTeams();
        Team GetTeamById(int teamId);
        void CreateTeam(Team team);
        void DeleteTeam(int teamId);
        void UpdateTeam(Team team);
    }
}
